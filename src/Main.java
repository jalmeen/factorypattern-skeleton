import connection.ConnectionFactory;
import connection.ConnectionManager;

public class Main {

    public static void main(String[] args) {

        ConnectionFactory connectionFactory = new ConnectionFactory();

        ConnectionManager c = connectionFactory.getConnection("MONGODB");
        c.goConnect();

        ConnectionManager c1 = connectionFactory.getConnection("JDBC");
        c1.goConnect();

        ConnectionManager c2 = connectionFactory.getConnection("CASSANDRA");
        c2.goConnect();
    }
}
