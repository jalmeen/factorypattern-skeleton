package crud;

import connection.CassandraConnection;

public class CassandraCrud extends CassandraConnection {
    public void create(){
        System.out.println("Document created!");
    }

    public void read(){
        System.out.println("Document is read!");
    }

    public void update(){
        System.out.println("Document updated!");
    }

    public void delete(){
        System.out.println("Document is deleted!");
    }
}
