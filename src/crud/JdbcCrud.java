package crud;

import connection.JdbcConnection;

public class JdbcCrud extends JdbcConnection {
    public void createRecord(){
        System.out.println("Document created!");
    }

    public void readRecord(){
        System.out.println("Document is read!");
    }

    public void updateRecord(){
        System.out.println("Document updated!");
    }

    public void deleteRecord(){
        System.out.println("Document is deleted!");
    }
}
