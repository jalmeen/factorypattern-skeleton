package crud;

import connection.MongoDbConnection;

public class MongoDbCrud extends MongoDbConnection {

    public void createDocument(){
        System.out.println("Document created!");
    }

    public void readDocument(){
        System.out.println("Document is read!");
    }

    public void updateDocument(){
        System.out.println("Document updated!");
    }

    public void deleteDocument(){
        System.out.println("Document is deleted!");
    }
}
