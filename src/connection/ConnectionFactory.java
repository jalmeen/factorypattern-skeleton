package connection;

import connection.CassandraConnection;

public class ConnectionFactory {

    public ConnectionManager getConnection(String connectionType){
        if(connectionType == null){
            return null;
        }
        if(connectionType.equalsIgnoreCase("JDBC")){
            return new JdbcConnection();

        } else if(connectionType.equalsIgnoreCase("MONGODB")){
            return new MongoDbConnection();

        } else if(connectionType.equalsIgnoreCase("CASSANDRA")){
            return new CassandraConnection();
        }

        return null;
    }
}
